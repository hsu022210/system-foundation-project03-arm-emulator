.global find_str_a
.func find_str_a

find_str_a:
	sub sp, sp, #16
        str r4, [sp]
        str r6, [sp, #4]
	str r1, [sp, #8]
	str r5, [sp, #12]
        mov r2, #0
	mov r3, #0
	mov r5, #-1

loop:
	ldrb r4, [r0]
        cmp r4, #0
        beq done

	ldrb r6, [r1]
	cmp r4, r6
	moveq r5, r2
	addeq r1, r1, #1
	addeq r3, r3, #1
	cmp r4, r6
	movne r3, #0
	ldrne r1, [sp, #8]
        movne r5, #-1

	ldrb r6, [r1]
	cmp r6, #0
	beq haveSubDone

        add r0, r0, #1
        add r2, r2, #1
        b loop

done:
	mov r0, r5

	ldr r6, [sp]
        ldr r4, [sp]
        add sp, sp, #16
        bx lr

haveSubDone:
	mov r0, r5
	sub r0, r0, r3
	add r0, r0, #1

	ldr r5, [sp, #12]
	ldr r1, [sp, #8]
	ldr r6, [sp, #4]
	ldr r4, [sp]
	add sp, sp, #16
	bx lr
