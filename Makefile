all : armemu

find_max.o : find_max.s
	as -o find_max.o find_max.s

sum_array.o : sum_array.s
	as -o sum_array.o sum_array.s

fib_iter.o : fib_iter.s
	as -o fib_iter.o fib_iter.s

fib_rec.o : fib_rec.s
	as -o fib_rec.o fib_rec.s

find_str.o : find_str.s
	as -o find_str.o find_str.s

armemu : armemu.c find_max.o sum_array.o fib_iter.o fib_rec.o find_str.o
	gcc -g -o armemu armemu.c find_max.o sum_array.o fib_iter.o fib_rec.o find_str.o

clean :
	rm -rf armemu *.o
