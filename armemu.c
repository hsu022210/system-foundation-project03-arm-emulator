#include <stdbool.h>
#include <stdio.h>
#include <time.h>

#define NREGS 16
#define STACK_SIZE 1024
#define SP 13
#define LR 14
#define PC 15
#define ADD -1
#define SUB -2
#define MOV -3
#define CMP -4
#define MVN -5
#define B -6
#define BL -7
#define LDR -8
#define STR -9
#define LDRB -10
#define CPSR_GT 0x00000000
#define CPSR_LT 0x80000000
#define CPSR_EQ 0x40000000
#define ITERS 100

int find_max_a(int *array, int n);
int sum_array_a(int *array, int n);
int fib_iter_a(int n);
int fib_rec_a(int n);
int find_str_a(char *s, char *sub);

unsigned int get_rd(unsigned int iw);
unsigned int get_rn(unsigned int iw);
unsigned int get_i(unsigned int iw);
signed int get_imm24(unsigned int iw);
void print_time_analysis(struct timespec time1, struct timespec time2);
struct timespec t1, t2;
long total_nsecs = 0;
time_t total_secs = 0;
double total_time = 0.0;
double inner_loop_usecs = 0.0;

struct arm_state {
    unsigned int regs[NREGS];
    unsigned int cpsr;
    unsigned char stack[STACK_SIZE];
    int num_of_inst_exec;
    int num_of_inst_computation;
    int num_of_inst_memory;
    int num_of_inst_branches;
    int array_of_registers_read[NREGS];
    int array_of_registers_written[NREGS];
    int num_of_branches_taken;
    int num_of_branches_not_taken;
};

void init_arm_state(struct arm_state *as, unsigned int *func,
                   unsigned int arg0, unsigned int arg1)
{
    int i;

    for (i = 0; i < NREGS; i++) {
        as->regs[i] = 0;
    }

    as->cpsr = 0;

    for (i = 0; i < STACK_SIZE; i++) {
        as->stack[i] = 0;
    }

    as->regs[PC] = (unsigned int) func;
    as->regs[SP] = (unsigned int) &as->stack[STACK_SIZE];
    as->regs[LR] = 0;

    as->regs[0] = arg0;
    as->regs[1] = arg1;

    as->num_of_inst_exec = 0;
    as->num_of_inst_computation = 0;
    as->num_of_inst_memory = 0;
    as->num_of_inst_branches = 0;

    for (i = 0; i < NREGS; i++) {
        as->array_of_registers_read[i] = 0;
    }
    as->array_of_registers_read[15] = 1;

    for (i = 0; i < NREGS; i++) {
        as->array_of_registers_written[i] = 0;
    }
    as->array_of_registers_written[15] = 1;

    as->num_of_branches_taken = 0;
    as->num_of_branches_not_taken = 0;

}

bool is_dp_inst(unsigned int iw)
{
    unsigned int op;
    op = (iw >> 26) & 0b11;
    return (op == 0);
}

bool is_add_inst(unsigned int iw)
{
    unsigned int op, opcode;

    op = (iw >> 26) & 0b11;
    opcode = (iw >> 21) & 0b1111;

    return (op == 0) && (opcode == 0b0100);
}

bool is_sub_inst(unsigned int iw)
{
    unsigned int op, opcode;

    op = (iw >> 26) & 0b11;
    opcode = (iw >> 21) & 0b1111;

    return (op == 0) && (opcode == 0b0010);
}

bool is_mov_inst(unsigned int iw)
{
    unsigned int op, opcode;

    op = (iw >> 26) & 0b11;
    opcode = (iw >> 21) & 0b1111;

    return (op == 0) && (opcode == 0b1101);
}

bool is_mvn_inst(unsigned int iw)
{
    unsigned int op, opcode;

    op = (iw >> 26) & 0b11;
    opcode = (iw >> 21) & 0b1111;

    return (op == 0) && (opcode == 0b1111);
}

bool is_cmp_inst(unsigned int iw)
{
    unsigned int op, opcode;

    op = (iw >> 26) & 0b11;
    opcode = (iw >> 21) & 0b1111;

    return (op == 0) && (opcode == 0b1010);
}

void armemu_dp_set(struct arm_state *state, unsigned int rd, unsigned int rn, unsigned int i, unsigned int iw, int type)
{
    unsigned int src2, src2_reg;
    signed int signed_src2;
    int sub_result;

    if ((type == ADD) || (type == SUB) || (type == CMP)) {

        if (i == 1){
            src2 = iw & 0xFF;
        }else{
            src2_reg = iw & 0xF;
            src2 = state->regs[src2_reg];
            state->array_of_registers_read[src2_reg] = 1;
        }

        if (type == ADD) {
            state->regs[rd] = state->regs[rn] + src2;
            state->array_of_registers_written[rd] = 1;
        } else if (type == SUB){
            state->regs[rd] = state->regs[rn] - src2;
            state->array_of_registers_written[rd] = 1;
        } else if (type == CMP){
            sub_result = state->regs[rn] - src2;
            if (sub_result == 0) {
                state->cpsr = CPSR_EQ;
            }else if (sub_result < 0){
                state->cpsr = CPSR_LT;
            }else if (sub_result > 0){
                state->cpsr = CPSR_GT;
            }
        }
        state->array_of_registers_read[rn] = 1;

    } else if ((type == MOV) ||(type == MVN)){

        if (i == 1){
            signed_src2 = iw & 0xFF;
            if (type == MVN) {
                signed_src2 = signed_src2 ^ 0xFFFFFFFF;
            }
        }else{
            src2_reg = iw & 0xF;
            signed_src2 = state->regs[src2_reg];
            state->array_of_registers_read[src2_reg] = 1;
        }

        state->regs[rd] = signed_src2;
        state->array_of_registers_written[rd] = 1;
    } else {
        printf("No type matched in armemu_dp\n");
    }
}

bool is_bx_inst(unsigned int iw)
{
    unsigned int bx_code;
    bx_code = (iw >> 4) & 0x00FFFFFF;
    return (bx_code == 0b000100101111111111110001);
}

void armemu_bx(struct arm_state *state, unsigned int iw)
{
    unsigned int rn;
    rn = iw & 0b1111;

    state->num_of_branches_taken += 1;
    state->regs[PC] = state->regs[rn];
}

bool is_b_inst(unsigned int iw)
{
    unsigned int op, funct;

    op = (iw >> 26) & 0b11;
    funct = (iw >> 24) & 0b11;

    return (op == 0b10) && (funct == 0b10);
}

bool is_bl_inst(unsigned int iw)
{
    unsigned int op, funct;

    op = (iw >> 26) & 0b11;
    funct = (iw >> 24) & 0b11;

    return (op == 0b10) && (funct == 0b11);
}

void armemu_branch_set(struct arm_state *state, signed int imm24, unsigned int iw, int type)
{
    state->num_of_branches_taken += 1;
    if (type == BL) {
        state->regs[LR] = state->regs[PC] + 8 - 4;
    }
    state->regs[PC] = state->regs[PC] + 8 + (imm24<<2);
}

bool is_mem_inst(unsigned int iw)
{
    unsigned int op;
    op = (iw >> 26) & 0b11;
    return (op == 0b01);
}

bool is_ldr_offset_inst(unsigned int iw)
{
    unsigned int op, p, bwl;

    op = (iw >> 26) & 0b11;
    p = (iw >> 24) & 0b1;
    bwl = (iw >> 20) & 0b111;

    return (op == 0b01) && (p == 0b1) && (bwl == 0b001);
}

bool is_str_offset_inst(unsigned int iw)
{
    unsigned int op, p, bwl;

    op = (iw >> 26) & 0b11;
    p = (iw >> 24) & 0b1;
    bwl = (iw >> 20) & 0b111;

    return (op == 0b01) && (p == 0b1) && (bwl == 0b000);
}

bool is_ldrb_offset_inst(unsigned int iw)
{
    unsigned int op, p, bwl;

    op = (iw >> 26) & 0b11;
    p = (iw >> 24) & 0b1;
    bwl = (iw >> 20) & 0b111;

    return (op == 0b01) && (p == 0b1) && (bwl == 0b101);
}

void armemu_mem_offset_set(struct arm_state *state, unsigned int rd, unsigned int rn, unsigned int i, unsigned int iw, int type)
{
    unsigned int src2, src2_reg;

    if (i == 1){
        src2_reg = iw & 0xF;
        src2 = state->regs[src2_reg];
        state->array_of_registers_read[src2_reg] = 1;
    }else{
        src2 = iw & 0xFFF;
    }

    if ((type == LDR) || (type == LDRB)) {
        if (type == LDR) {
            state->regs[rd] = *((unsigned int *)(state->regs[rn] + src2));
        } else{
            state->regs[rd] =  (unsigned int) (*(char *)(state->regs[rn] + src2));
        }
        state->array_of_registers_read[rn] = 1;
        state->array_of_registers_written[rd] = 1;
    } else if (type == STR){
        *((unsigned int *)(state->regs[rn] + src2)) = state->regs[rd];
        state->array_of_registers_read[rd] = 1;
        state->array_of_registers_written[rn] = 1;
    } else {
        printf("No type matched in armemu_mem\n");
    }
}

unsigned int get_rd(unsigned int iw)
{
    return (iw >> 12) & 0xF;
}

unsigned int get_rn(unsigned int iw)
{
    return (iw >> 16) & 0xF;
}

unsigned int get_i(unsigned int iw)
{
    return (iw >> 25) & 0b1;
}

signed int get_imm24(unsigned int iw)
{
    signed int imm24;
    imm24 = iw & 0xFFFFFF;
    imm24 = (imm24<<8) >>8;
    return imm24;
}

bool is_eq_inst(unsigned int iw)
{
    unsigned int cond;
    cond = (iw >> 28) & 0xF;
    return (cond == 0b0000);
}

bool is_lt_inst(unsigned int iw)
{
    unsigned int cond;
    cond = (iw >> 28) & 0xF;
    return (cond == 0b1011);
}

bool is_le_inst(unsigned int iw)
{
    unsigned int cond;
    cond = (iw >> 28) & 0xF;
    return (cond == 0b1011 || cond == 0b0000);
}

bool is_gt_inst(unsigned int iw)
{
    unsigned int cond;
    cond = (iw >> 28) & 0xF;
    return (cond == 0b1100);
}

bool is_ne_inst(unsigned int iw)
{
    unsigned int cond;
    cond = (iw >> 28) & 0xF;
    return (cond == 0b0001);
}

bool is_al_inst(unsigned int iw)
{
    unsigned int cond;
    cond = (iw >> 28) & 0xF;
    return (cond == 0b1110);
}

void armemu_one(struct arm_state *state)
{
    unsigned int iw, rd, rn, i;
    signed int imm24;

    iw = *((unsigned int *) state->regs[PC]);

    if ((is_eq_inst(iw) && (state->cpsr == CPSR_EQ)) || (is_le_inst(iw) && (state->cpsr != CPSR_GT))
        || (is_lt_inst(iw) && (state->cpsr == CPSR_LT)) || (is_gt_inst(iw) && (state->cpsr == CPSR_GT))
        || (is_ne_inst(iw) && (state->cpsr != CPSR_EQ)) || is_al_inst(iw)) {

            rd = get_rd(iw);
            rn = get_rn(iw);
            i = get_i(iw);

            if (is_b_inst(iw) || is_bl_inst(iw) || is_bx_inst(iw)) {
                imm24 = get_imm24(iw);

                if (is_bx_inst(iw)) {
                    armemu_bx(state, iw);
                } else if (is_bl_inst(iw)) {
                    armemu_branch_set(state, imm24, iw, BL);
                } else if (is_b_inst(iw)) {
                    armemu_branch_set(state, imm24, iw, B);
                }
                state->num_of_inst_branches += 1;

            } else if (is_dp_inst(iw)) {

                if (is_sub_inst(iw)) {
                    armemu_dp_set(state, rd, rn, i, iw, SUB);
                } else if (is_cmp_inst(iw)) {
                    armemu_dp_set(state, rd, rn, i, iw, CMP);
                } else if (is_add_inst(iw)) {
                    armemu_dp_set(state, rd, rn, i, iw, ADD);
                } else if (is_mov_inst(iw)) {
                    armemu_dp_set(state, rd, rn, i, iw, MOV);
                } else if (is_mvn_inst(iw)) {
                    armemu_dp_set(state, rd, rn, i, iw, MVN);
                }
                if (rd != PC) {
                    state->regs[PC] = state->regs[PC] + 4;
                }
                state->num_of_inst_computation += 1;

            } else if (is_mem_inst(iw)){

               if (is_str_offset_inst(iw)){
                   armemu_mem_offset_set(state, rd, rn, i, iw, STR);
               } else if (is_ldrb_offset_inst(iw)){
                   armemu_mem_offset_set(state, rd, rn, i, iw, LDRB);
               } else if (is_ldr_offset_inst(iw)){
                   armemu_mem_offset_set(state, rd, rn, i, iw, LDR);
               }
               if (rd != PC) {
                   state->regs[PC] = state->regs[PC] + 4;
               }
               state->num_of_inst_memory += 1;

           } else {
               printf("instruction not defined\n");
           }

    } else {

        if (is_b_inst(iw) || is_bl_inst(iw) || is_bx_inst(iw)) {
            state->num_of_inst_branches += 1;
            state->num_of_branches_not_taken += 1;
        } else if (is_dp_inst(iw)){
            state->num_of_inst_computation += 1;
        } else if (is_mem_inst(iw)){
            state->num_of_inst_memory += 1;
        } else {
            printf("instruction not defined\n");
        }
        state->regs[PC] = state->regs[PC] + 4;
    }
    state->num_of_inst_exec += 1;
}


unsigned int armemu(struct arm_state *state)
{
    while (state->regs[PC] != 0) {
        armemu_one(state);
    }
    return state->regs[0];
}

void dynamic_analysis(struct arm_state *state)
{
    int i;
    float per_of_inst_computation, per_of_inst_memory, per_of_inst_branches, per_of_branches_taken, per_of_branches_not_taken;

    per_of_inst_computation = (float) state->num_of_inst_computation / (float) state->num_of_inst_exec * 100;
    per_of_inst_memory = (float) state->num_of_inst_memory / (float) state->num_of_inst_exec * 100;
    per_of_inst_branches = (float) state->num_of_inst_branches / (float) state->num_of_inst_exec * 100;
    per_of_branches_taken = (float) state->num_of_branches_taken / (float) state->num_of_inst_branches * 100;
    per_of_branches_not_taken = (float) state->num_of_branches_not_taken / (float) state->num_of_inst_branches * 100;

    printf("---------------------------------------------------- dynamic_analysis ----------------------------------\n");
    printf("num_of_inst_exec: %d\n", state->num_of_inst_exec);
    printf("num_of_inst_computation: %d (%f%%)\n", state->num_of_inst_computation, per_of_inst_computation);
    printf("num_of_inst_memory: %d (%f%%)\n", state->num_of_inst_memory, per_of_inst_memory);
    printf("num_of_inst_branches: %d (%f%%)\n", state->num_of_inst_branches, per_of_inst_branches);
    printf("num_of_branches_taken: %d (%f%%)\n", state->num_of_branches_taken, per_of_branches_taken);
    printf("num_of_branches_not_taken: %d (%f%%)\n", state->num_of_branches_not_taken, per_of_branches_not_taken);

    printf("Registers read: ");
    for (i = 0; i < NREGS; i++) {
        if (state->array_of_registers_read[i] == 1) {
            printf("r%d ", i);
        }
    }
    printf("\n");

    printf("Registers written: ");
    for (i = 0; i < NREGS; i++) {
        if (state->array_of_registers_written[i] == 1) {
            printf("r%d ", i);
        }
    }
    printf("\n");
    printf("\n");
}

void test_interface(struct arm_state state, int func_name, int *array, int array_size, int fib_input, char *s, char *sub)
{
    int result, i;
    char *s_func_name;

    // native
    if (func_name == (int)find_max_a) {

        s_func_name = "find_max_a";
        clock_gettime(CLOCK_MONOTONIC, &t1);
        for (i = 0; i < ITERS; i++) {
            result = find_max_a(array, array_size);
        }
        clock_gettime(CLOCK_MONOTONIC, &t2);

    } else if (func_name == (int)sum_array_a){

        s_func_name = "sum_array_a";
        clock_gettime(CLOCK_MONOTONIC, &t1);
        for (i = 0; i < ITERS; i++) {
            result = sum_array_a(array, array_size);
        }
        clock_gettime(CLOCK_MONOTONIC, &t2);

    } else if (func_name == (int)fib_iter_a){

        s_func_name = "fib_iter_a";
        clock_gettime(CLOCK_MONOTONIC, &t1);
        for (i = 0; i < ITERS; i++) {
            result = fib_iter_a(fib_input);
        }
        clock_gettime(CLOCK_MONOTONIC, &t2);

    } else if (func_name == (int)fib_rec_a){

        s_func_name = "fib_rec_a";
        clock_gettime(CLOCK_MONOTONIC, &t1);
        for (i = 0; i < ITERS; i++) {
            result = fib_rec_a(fib_input);
        }
        clock_gettime(CLOCK_MONOTONIC, &t2);

    } else if (func_name == (int)find_str_a){

        s_func_name = "find_str_a";
        clock_gettime(CLOCK_MONOTONIC, &t1);
        for (i = 0; i < ITERS; i++) {
            result = find_str_a(s, sub);
        }
        clock_gettime(CLOCK_MONOTONIC, &t2);

    } else {
        printf("No such function exist\n");
    }

    if (func_name == (int)sum_array_a || func_name == (int)find_max_a) {
        printf("==================================================== test_for_%s ================= array from %d to %d =================\n", s_func_name, array[0], array[array_size-1]);
        printf("%s_native = %d\n", s_func_name, result);
    } else if(func_name == (int)fib_iter_a || func_name == (int)fib_rec_a){
        printf("==================================================== test_for_%s ================= (%d) ========================\n", s_func_name, fib_input);
        printf("%s_native(%d) = %d\n",s_func_name, fib_input, result);
    } else {
        printf("==================================================== test_for_%s ================= %s, %s =====================\n",s_func_name, s, sub);
        printf("%s_native(%s, %s) = %d\n", s_func_name, s, sub, result);
    }
    print_time_analysis(t1, t2);

    // emulator
    if (func_name == (int)sum_array_a || func_name == (int)find_max_a) {

        clock_gettime(CLOCK_MONOTONIC, &t1);
        for (i = 0; i < ITERS; i++) {
            init_arm_state(&state, (unsigned int *) func_name, (unsigned int) array, array_size);
            result = armemu(&state);
        }
        clock_gettime(CLOCK_MONOTONIC, &t2);
        printf("%s_emulate = %d\n", s_func_name, result);

    } else if(func_name == (int)fib_iter_a || func_name == (int)fib_rec_a){

        clock_gettime(CLOCK_MONOTONIC, &t1);
        for (i = 0; i < ITERS; i++) {
            init_arm_state(&state, (unsigned int *) func_name, fib_input, 0);
            result = armemu(&state);
        }
        clock_gettime(CLOCK_MONOTONIC, &t2);
        printf("%s_emulate(%d) = %d\n",s_func_name, fib_input, result);

    } else {

        clock_gettime(CLOCK_MONOTONIC, &t1);
        for (i = 0; i < ITERS; i++) {
            init_arm_state(&state, (unsigned int *) func_name, (unsigned int) s, (unsigned int) sub);
            result = armemu(&state);
        }
        clock_gettime(CLOCK_MONOTONIC, &t2);
        printf("%s_emulate(%s, %s) = %d\n", s_func_name, s, sub, result);
    }
    print_time_analysis(t1, t2);

    // analysis
    dynamic_analysis(&state);
}

void print_time_analysis(struct timespec time1, struct timespec time2)
{
    total_secs = time2.tv_sec - time1.tv_sec;
    total_nsecs = time2.tv_nsec - time1.tv_nsec;
    printf("--------- total_secs = %ld ---------", total_secs);
    printf("total_nsecs = %ld ---------", total_nsecs);

    total_time = (double) total_secs + ((double) total_nsecs) / 1000000000.0;
    printf("total_time = %lf ---------", total_time);

    inner_loop_usecs = (((double) total_time) / ((double) ITERS)) * 1000000.0;
    printf("function_single_run_spend_usecs = %lf ---------\n", inner_loop_usecs);
}

int main(int argc, char **argv)
{
    struct arm_state state;
    int array_size = 1000;
    int array[array_size];
    int j;
    char *s;
    char *sub;

    // ------------------------------------------find_max test1, array from 1 to 1000
    for (j = 0; j < array_size; j++) {
       array[j] = j+1;
    }
    test_interface(state, (int)find_max_a, array, array_size, 0, NULL, NULL);

    // ------------------------------------------find_max test2, array from -1000 to -1
    for (j = 0; j < array_size; j++) {
       array[j] = j-1000;
    }
    test_interface(state, (int)find_max_a, array, array_size, 0, NULL, NULL);

    // ------------------------------------------find_max test3, array from -500 to 499
    for (j = 0; j < array_size; j++) {
       array[j] = j-500;
    }
    test_interface(state, (int)find_max_a, array, array_size, 0, NULL, NULL);

    // ------------------------------------------find_max test4, array from -250 to 749
    for (j = 0; j < array_size; j++) {
       array[j] = j-250;
    }
    test_interface(state, (int)find_max_a, array, array_size, 0, NULL, NULL);

    // ------------------------------------------sum_array test1, array from 1 to 1000
    for (j = 0; j < array_size; j++) {
       array[j] = j+1;
    }
    test_interface(state, (int)sum_array_a, array, array_size, 0, NULL, NULL);

    // ------------------------------------------sum_array test2, array from -1000 to -1
    for (j = 0; j < array_size; j++) {
       array[j] = j-1000;
    }
    test_interface(state, (int)sum_array_a, array, array_size, 0, NULL, NULL);

    // ------------------------------------------sum_array test3, array from -500 to 499
    for (j = 0; j < array_size; j++) {
       array[j] = j-500;
    }
    test_interface(state, (int)sum_array_a, array, array_size, 0, NULL, NULL);

    // ------------------------------------------sum_array test4, array from -250 to 749
    for (j = 0; j < array_size; j++) {
       array[j] = j-250;
    }
    test_interface(state, (int)sum_array_a, array, array_size, 0, NULL, NULL);

    // ------------------------------------------fib_iter test, input 10 & 19
    test_interface(state, (int)fib_iter_a, NULL, 0, 10, NULL, NULL);
    test_interface(state, (int)fib_iter_a, NULL, 0, 19, NULL, NULL);

    // ------------------------------------------fib_rec test, input 10 & 19
    test_interface(state, (int)fib_rec_a, NULL, 0, 10, NULL, NULL);
    test_interface(state, (int)fib_rec_a, NULL, 0, 19, NULL, NULL);

    // ------------------------------------------find_str test, input hamburger, burger
    s = "hamburger";
    sub = "burger";
    test_interface(state, (int)find_str_a, NULL, 0, 0, s, sub);

    // ------------------------------------------find_str test, input hamburger, beef
    sub = "beef";
    test_interface(state, (int)find_str_a, NULL, 0, 0, s, sub);

    return 0;
}
