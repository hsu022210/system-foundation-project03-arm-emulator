.global fib_iter_a
.func fib_iter_a
 
fib_iter_a:
	sub sp, sp, #8
        str r4, [sp]

	mov r1, #0
        mov r2, #0
	mov r3, #1
	mov r4, #0

loop:
	cmp r2, #1
	movle r4, r2
	addgt r4, r1, r3
        movgt r1, r3
        movgt r3, r4

	cmp r2, r0
        beq done

        add r2, r2, #1
        b loop

done:   
	mov r0, r4
	ldr r4, [sp]
        add sp, sp, #8
        bx lr

